<!DOCTYPE html>
<html>
<head>
    <title>Scanner un produit</title>
</head>
<body>
    <h1>Scanner un produit</h1>

    <form method="post" action="">
    <label for="code-barre">Code-barre :</label>
    <input type="text" name="code-barre" id="code-barre" autofocus>
   
</form>

    <?php
    // Démarrage de la session
    session_start();

    // Connexion à la base de données
    $bdd = new PDO('mysql:host=localhost;dbname=runtouz;charset=utf8', 'RunTouz', 'RunTouz2023');
    // Initialisation du panier
    if (!isset($_SESSION['panier'])) {
        $_SESSION['panier'] = array();
    }
    if (isset($_POST['code-barre'])) {
        // Requête SQL pour récupérer les informations du produit correspondant au code-barre
        $req = $bdd->prepare('SELECT * FROM produits WHERE code_barre = :code_barre');
        $req->execute(array('code_barre' => $_POST['code-barre']));
        $produit = $req->fetch();

        if ($produit) {
            // Vérifie si le produit est déjà dans le panier
            if (array_key_exists($produit['code_barre'], $_SESSION['panier'])) {
                // Si le produit est déjà dans le panier, on incrémente sa quantité
                $_SESSION['panier'][$produit['code_barre']]['quantite']++;
            } else {
                if(isset($_POST['quantite']) && is_numeric($_POST['quantite']) && $_POST['quantite']>0){
                    $quantite = $_POST['quantite'];
                }else{
                    $quantite = 1;
                }
                
                if (array_key_exists($produit['code_barre'], $_SESSION['panier'])) {
                    // Si le produit est déjà dans le panier, on incrémente sa quantité
                    $_SESSION['panier'][$produit['code_barre']]['quantite'] += $quantite;
                } else {
                    // Sinon, on ajoute le produit au panier avec la quantité saisie
                    $_SESSION['panier'][$produit['code_barre']] = array(
                        'quantite' => $quantite,
                        'prix' => $produit['prix'],
                        'marque' => $produit['marque'],
                        'nom' => $produit['nom'],
                    );
                    
                }
                  
            }
            
           // Affichage du tableau avec les informations du produit
echo '<h2>Informations du produit</h2>';
echo '<table>';
echo '<tr><th>Code</th><th>Prix</th><th>Nom</th><th>Marque</th><th>Quantité</th><th>Total</th><th></th></tr>';
foreach ($_SESSION['panier'] as $code_barre => $produit_panier) {
    $total_produit = $produit_panier['quantite'] * $produit_panier['prix'];
    echo '<tr>';
    echo '<td>'.$code_barre.'</td>';
    echo '<td>'.$produit_panier['prix'].' €</td>';
    echo '<td>'.$produit_panier['nom'].'</td>';
    echo '<td>'.$produit_panier['marque'].'</td>';
    echo '<td><form method="post"><input type="hidden" name="code-barre-modification" value="'.$code_barre.'"><button type="submit" name="quantite-modification" value="-1">-</button>'.$produit_panier['quantite'].'<button type="submit" name="quantite-modification" value="1">+</button></form></td>';
    echo '<td>'.$total_produit.' €</td>';
    echo '<td><form method="post"><input type="hidden" name="code-barre-suppression" value="'.$code_barre.'"><button type="submit">Supprimer</button></form></td>';
    echo '</tr>';
}
echo '</table>';

// Gestion des modifications de quantité
if (isset($_POST['code-barre-modification']) && isset($_POST['quantite-modification'])) {
    $code_barre = $_POST['code-barre-modification'];
    $modification = $_POST['quantite-modification'];
    if (isset($_SESSION['panier'][$code_barre])) {
        $_SESSION['panier'][$code_barre]['quantite'] += $modification;
        if ($_SESSION['panier'][$code_barre]['quantite'] <= 0) {
            unset($_SESSION['panier'][$code_barre]);
        }
    }
}

// Affichage du total du panier et du bouton "Valider"
if (!empty($_SESSION['panier'])) {
    $total_panier = 0;
    foreach ($_SESSION['panier'] as $code_barre => $produit_panier) {
        $total_produit = $produit_panier['quantite'] * $produit_panier['prix'];
        $total_panier += $total_produit;
    }
    echo '<p>Total du panier : '.$total_panier.' €</p>';
    echo '<form method="post" action="paiement.html">';
    echo '<button type="submit" name="valider-panier"'.($total_panier <= 0 ? ' disabled' : '').'>Valider</button>';
    echo '</form>';
} else {
    echo '<p>Le panier est vide.</p>';
}

// Gestion de la validation du panier
if (isset($_POST['valider-panier']) && !empty($_SESSION['panier'])) {
    header('Location: paiement.html');
    exit;
}

}
}



?>